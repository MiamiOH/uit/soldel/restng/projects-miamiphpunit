<?php

class miamiUnit extends PHPUnit_Framework_TestCase{

    protected $config = array();
    protected $resourceBeingCalledName = '';
    protected $resourceBeingCalledArgs = array();

    protected function setUp()
    {
        $this->config = array();
        $this->resourceBeingCalledName = '';
        $this->resourceBeingCalledArgs = array();
    }

    public function callResourceWithName($subject)
    {
        $this->resourceBeingCalledName = $subject;
        return true;
    }

    public function callResourceWithArgs($subject)
    {
        $this->resourceBeingCalledArgs = $subject;
        return true;
    }

    public function callResourceFailureMock()
    {
        $response = new \RESTng\Util\Response();
        $response->setStatus(\RESTng\API_FAILED);
        $response->setPayload(array('data' => array('message', '')));
        return $response;
    }

    public function getConfigurationMock()
    {
        return $this->config;
    }

    public function getDataSourceMock()
    {
        return $this->mockDataSource;
    }

    public function authApplication($subject)
    {
        $this->authApplicationValue = $subject;
        return true;
    }

    public function authModule($subject)
    {
        $this->authModuleValue = $subject;
        return true;
    }

    public function authGrantKey($subject)
    {
        $this->authGrantKeyValue = $subject;
        return true;
    }

    public function authPermissionMock()
    {
        $permissionString = $this->authApplicationValue . '.' . $this->authModuleValue . '.' . $this->authGrantKeyValue;
        if (array_key_exists($permissionString, $this->permissionArray)) {
            return ($this->permissionArray[$permissionString]);
        } else {
            return (1);
        }

    }
}